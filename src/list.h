#ifndef LIST_H
#define LIST_H

/* Singly-linked list node */
struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(nullptr) {}
};

void deleteNode(ListNode *node);

#endif /* list.h */
