#ifndef LEET_CODE_TREE_H
#define LEET_CODE_TREE_H

#include "../tree.h"

TreeNode *invertTree(TreeNode *root);

#endif /* leet_code/tree.h */
