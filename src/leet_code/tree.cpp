#include "tree.h"

TreeNode *invertTree(TreeNode *root) {
  if (root) {

    TreeNode *temp = root->left;
    root->left = root->right;
    root->right = temp;

    invertTree(root->right);
    invertTree(root->left);
    return root;
  } else {
    return nullptr;
  }
}
