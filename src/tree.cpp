#include "tree.h"
#include <iostream>

void inorderPrint(TreeNode *root) {
  if (root != nullptr) {
    inorderPrint(root->left);
    std::cout << root->val << " ";
    inorderPrint(root->right);
  }
}
