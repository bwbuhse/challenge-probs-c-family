#include "twostrings.h"
#include <iostream>
#include <unordered_map>

#define ULDIFF 32

string work_on_strings(std::string &a, std::string &b) {
  unordered_map<char, int> a_chars = unordered_map<char, int>();
  unordered_map<char, int> b_chars = unordered_map<char, int>();

  for (char c : a) {
    a_chars.insert_or_assign(c, a_chars[c] ^ 0b1);
    cout << a_chars[c] << endl;
  }

  for (char c : b) {
    b_chars.insert_or_assign(c, b_chars[c] ^ 0b1);
  }

  for (auto it : a_chars) {
    if (it.second) {
      for (int i = 0; i < a.size(); i++) {
        if (a[i] == it.first) {
          if (a[i] < 'a') {
            // If we get here we know it's uppercase
            a[i] += ULDIFF;
          } else {
            a[i] -= ULDIFF;
          }
        }
      }
    }
  }

  for (auto it : b_chars) {
    if (it.second) {
      for (int i = 0; i < b.size(); i++) {
        if (b[i] == it.first) {
          if (b[i] < 'a') {
            // If we get here we know it's uppercase
            b[i] += ULDIFF;
          } else {
            b[i] -= ULDIFF;
          }
        }
      }
    }
  }

  return a + b;
}
