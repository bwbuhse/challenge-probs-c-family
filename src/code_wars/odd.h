#include <vector>

// https://www.codewars.com/kata/54da5a58ea159efa38000836/
int findOdd(const std::vector<int> &numbers);
