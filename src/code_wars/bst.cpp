#include "bst.h"
#include <bits/stdc++.h>
#include <iostream>

using namespace std;

TreeNode *bstFromPreorderHelper(vector<int> &preorder, int &curIndex,
                                int minValue, int maxValue) {
  // Base case
  if (curIndex >= preorder.size()) {
    return nullptr;
  }

  int key = preorder[curIndex];
  TreeNode *curNode = nullptr;

  if (key > minValue && key < maxValue) {
    curNode = new TreeNode(key);
    curIndex += 1;
    if (curIndex < preorder.size()) {
      // Create the subtrees
      curNode->left = bstFromPreorderHelper(preorder, curIndex, minValue, key);
      curNode->right = bstFromPreorderHelper(preorder, curIndex, key, maxValue);
    }
  }

  return curNode;
}

TreeNode* bstFromPreorder(vector<int>& preorder) {
  int index = 0;
  // Fills in the BST using recursion
  TreeNode *root = bstFromPreorderHelper(preorder, index, INT_MIN, INT_MAX);

  return root;
}

