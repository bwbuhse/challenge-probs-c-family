#include "walk.h"

#define DISTANCE 10

using namespace std;

bool isValidWalk(vector<char> walk) {
  if (walk.size() != DISTANCE) {
    return false;
  }

  int x = 0, y = 0;
  for (auto step : walk) {
    switch (step) {
    case 'n':
      y++;
      break;
    case 's':
      y--;
      break;
    case 'e':
      x++;
      break;
    case 'w':
      x--;
      break;
    }
  }

  return !x && !y;
}
