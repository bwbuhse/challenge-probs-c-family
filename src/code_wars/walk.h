#ifndef CODE_WARS_WALK_H
#define CODE_WARS_WALK_H

#include <vector>

bool isValidWalk(std::vector<char> walk);

#endif
