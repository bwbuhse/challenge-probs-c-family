#include "odd.h"
#include <algorithm>

using namespace std;

int findOdd(const vector<int> &numbers) {
  for (auto number : numbers) {
    if ((count(numbers.begin(), numbers.end(), number) & 0b1) == 0b1) {
      return number;
    }
  }

  return -1;
}
