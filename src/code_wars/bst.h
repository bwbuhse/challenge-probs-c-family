#ifndef CODE_WARS_BST_H
#define CODE_WARS_BST_H

#include "../tree.h"
#include <vector>

TreeNode *bstFromPreorder(std::vector<int> &preorder);

void inorderPrint(TreeNode *root);

#endif // src/code_wars/bst.h
