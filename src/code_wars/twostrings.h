#ifndef CODE_WARS_TWOSTRINGS_H
#define CODE_WARS_TWOSTRINGS_H

#include <string>

using namespace std;

string work_on_strings(std::string &a, std::string &b);

#endif
