#include "code_wars/bst.h"
#include "code_wars/twostrings.h"
#include "code_wars/walk.h"
#include "leet_code/tree.h"
#include "list.h"
#include "tree.h"
#include <iostream>
#include <string.h>
#include <vector>

using namespace std;

void code_wars(char *);
void leet_code(char *);

int main(int argc, char *argv[]) {
  // Ensure valid parameters are sent
  if (argc == 1) {
    cout << "Please give the website and the challenge as parameters to the "
            "executable"
         << endl;
    return EXIT_FAILURE;
  } else if (argc == 2) {
    cout << "Please give the challenge as the second parameter (after the "
            "website) to the executable"
         << endl;
    return EXIT_FAILURE;
  }

  // Find the website
  char *website = argv[1];
  char *challenge = argv[2];
  if  (!strcmp(website, "code_wars")) {
    code_wars(challenge);
  } else if (!strcmp(website, "leet_code")) {
    leet_code(challenge);
  } else {
    cout << website << " is not a valid website option." << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

void leet_code(char *challenge) {
  if (!strcmp(challenge, "invert_tree")) {
    vector<int> v = vector<int>();
    v.push_back(4);
    v.push_back(2);
    v.push_back(1);
    v.push_back(3);
    v.push_back(7);
    v.push_back(6);
    v.push_back(9);

    // Just using this since I already have it made
    TreeNode *root = bstFromPreorder(v);

    inorderPrint(invertTree(root));
    cout << endl;
  } else if (!strcmp(challenge, "delete_node")) {
    cout << "IMPLEMENT THIS TEST" << endl;
  } else {
    cout << challenge << " is not a valid challenge for Leet Code." << endl;
  }
}

void code_wars(char* challenge) {
  if (!strcmp(challenge, "bst")) {
    vector<int> v = vector<int>();
    v.push_back(8);
    v.push_back(5);
    v.push_back(1);
    v.push_back(7);
    v.push_back(10);
    v.push_back(12);

    TreeNode *root = bstFromPreorder(v);
    inorderPrint(root);
    cout << endl;
  } else if (!strcmp(challenge, "walk")) {
    vector<char> v = vector<char>();
    v.push_back('n');
    v.push_back('s');
    v.push_back('n');
    v.push_back('s');
    v.push_back('n');
    v.push_back('s');
    v.push_back('n');
    v.push_back('s');
    v.push_back('n');
    v.push_back('s');

    cout << "Should be true: " << isValidWalk(v) << endl; // True

    v.pop_back();

    cout << "Should be false: " << isValidWalk(v) << endl; // False
  } else if (!strcmp(challenge, "twostrings")) {
    string a("abab");
    string b("bababa");
    cout << work_on_strings(a, b) << endl;
  } else {
    cout << challenge << " is not a valid challenge for Code Wars." << endl;
  }
}
